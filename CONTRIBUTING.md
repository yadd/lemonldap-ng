# Contributing to LemonLDAP::NG

* Repository, issues,... : https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng
* Translations :
  * software : https://www.transifex.com/lemonldapng/lemonldapng/
  * documentation : since 2.0, LLNG community supports only english doc