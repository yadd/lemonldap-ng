Description: fix open redirection
Author: Yadd <yadd@debian.org>
 Maxime Besson <maxime.besson@worteks.com>
Origin: upstream, https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng/-/merge_requests/342/diffs
Bug: https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng/-/issues/2931
Forwarded: not-needed
Applied-Upstream: 2.17.0, https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng/-/merge_requests/342
Last-Update: 2023-09-20

--- a/lemonldap-ng-handler/lib/Lemonldap/NG/Handler/ApacheMP2/Main.pm
+++ b/lemonldap-ng-handler/lib/Lemonldap/NG/Handler/ApacheMP2/Main.pm
@@ -16,6 +16,7 @@
 use APR::Table;
 use Apache2::Const -compile =>
   qw(FORBIDDEN HTTP_UNAUTHORIZED REDIRECT OK DECLINED DONE SERVER_ERROR AUTH_REQUIRED HTTP_SERVICE_UNAVAILABLE);
+use URI;
 use base 'Lemonldap::NG::Handler::Main';
 
 use constant FORBIDDEN         => Apache2::Const::FORBIDDEN;
@@ -166,7 +167,7 @@
         $f->r->status( $class->REDIRECT );
         $f->r->status_line("303 See Other");
         $f->r->headers_out->unset('Location');
-        $f->r->err_headers_out->set( 'Location' => $url );
+        $f->r->err_headers_out->set( 'Location' => URI->new($url)->as_string );
         $f->ctx(1);
     }
     while ( $f->read( my $buffer, 1024 ) ) {
--- a/lemonldap-ng-handler/lib/Lemonldap/NG/Handler/Main/Run.pm
+++ b/lemonldap-ng-handler/lib/Lemonldap/NG/Handler/Main/Run.pm
@@ -9,6 +9,7 @@
 
 #use AutoLoader 'AUTOLOAD';
 use MIME::Base64;
+use URI;
 use URI::Escape;
 use Lemonldap::NG::Common::Session;
 
@@ -697,7 +698,7 @@
     ) ? '' : ":$portString";
     my $url = "http" . ( $_https ? "s" : "" ) . "://$realvhost$portString$s";
     $class->logger->debug("Build URL $url");
-    return $url;
+    return URI->new($url)->as_string;
 }
 
 ## @rmethod protected int isUnprotected()
--- a/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/CDC.pm
+++ b/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/CDC.pm
@@ -9,6 +9,7 @@
 use Mouse;
 use MIME::Base64;
 use Lemonldap::NG::Common::FormEncode;
+use URI;
 
 our $VERSION = '2.0.6';
 
@@ -163,7 +164,10 @@
         );
 
         # Redirect
-        return [ 302, [ Location => $urldc, $req->spliceHdrs ], [] ];
+        return [
+            302, [ Location => URI->new($urldc)->as_string, $req->spliceHdrs ],
+            []
+        ];
 
     }
 
--- a/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Issuer/CAS.pm
+++ b/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Issuer/CAS.pm
@@ -13,6 +13,7 @@
   PE_BADURL
   PE_SENDRESPONSE
 );
+use URI;
 
 our $VERSION = '2.0.9';
 
@@ -84,7 +85,8 @@
     if ( $gateway and $gateway eq "true" ) {
         $self->logger->debug(
             "Gateway mode requested, redirect without authentication");
-        $req->response( [ 302, [ Location => $service ], [] ] );
+        $req->response(
+            [ 302, [ Location => URI->new($service)->as_string ], [] ] );
         for my $s ( $self->ipath, $self->ipath . 'Path' ) {
             $self->logger->debug("Removing $s from pdata")
               if delete $req->pdata->{$s};
--- a/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Lib/OpenIDConnect.pm
+++ b/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Lib/OpenIDConnect.pm
@@ -16,6 +16,7 @@
 use Lemonldap::NG::Common::UserAgent;
 use MIME::Base64 qw/encode_base64 decode_base64/;
 use Mouse;
+use URI;
 
 use Lemonldap::NG::Portal::Main::Constants qw(PE_OK PE_REDIRECT);
 
@@ -1684,7 +1685,7 @@
         $response_url .= build_urlencoded( state => $state );
     }
 
-    return $response_url;
+    return URI->new($response_url)->as_string;
 }
 
 # Create session_state parameter
--- a/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Lib/SAML.pm
+++ b/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Lib/SAML.pm
@@ -2493,7 +2493,7 @@
 
         # Redirect user to response URL
         my $slo_url = $logout->msg_url;
-        return [ 302, [ Location => $slo_url ], [] ];
+        return [ 302, [ Location => URI->new($slo_url)->as_string ], [] ];
     }
 
     # HTTP-POST
--- a/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Main/Process.pm
+++ b/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Main/Process.pm
@@ -132,6 +132,7 @@
                 $req->{urldc} =~ s/[\r\n]//sg;
             }
         }
+        $req->{urldc} = URI->new( $req->{urldc} )->as_string;
 
         # For logout request, test if Referer comes from an authorized site
         my $tmp = (
--- a/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Main/Run.pm
+++ b/lemonldap-ng-portal/lib/Lemonldap/NG/Portal/Main/Run.pm
@@ -402,7 +402,13 @@
                 $self->logger->info("Force cleaning pdata");
                 delete $req->{pdata}->{_url};
             }
-            return [ 302, [ Location => $req->{urldc}, $req->spliceHdrs ], [] ];
+            return [
+                302,
+                [
+                    Location => URI->new( $req->{urldc} )->as_string,
+                ],
+                []
+            ];
         }
     }
     my ( $tpl, $prms ) = $self->display($req);
--- a/lemonldap-ng-portal/t/03-XSS-protection.t
+++ b/lemonldap-ng-portal/t/03-XSS-protection.t
@@ -19,21 +19,25 @@
     '' => 0, 'Empty',
 
     # 2 http://test1.example.com/
-    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29tLw==' => 1, 'Protected virtual host',
+    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29tLw==' => 'http://test1.example.com/',
+    'Protected virtual host',
 
     # 3 http://test1.example.com
-    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29t' => 1, 'Missing / in URL',
+    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29t' => 'http://test1.example.com',
+    'Missing / in URL',
 
     # 4 http://test1.example.com:8000/test
-    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29tOjgwMDAvdGVzdA==' => 1,
+    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29tOjgwMDAvdGVzdA==' =>
+      'http://test1.example.com:8000/test',
     'Non default port',
 
     # 5 http://test1.example.com:8000/
-    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29tOjgwMDAv' => 1,
+    'aHR0cDovL3Rlc3QxLmV4YW1wbGUuY29tOjgwMDAv' =>
+      'http://test1.example.com:8000/',
     'Non default port with missing /',
 
     # 6 http://t.example2.com/test
-    'aHR0cDovL3QuZXhhbXBsZTIuY29tL3Rlc3Q=' => 1,
+    'aHR0cDovL3QuZXhhbXBsZTIuY29tL3Rlc3Q=' => 'http://t.example2.com/test',
     'Undeclared virtual host in trusted domain',
 
     # 7 http://testexample2.com/
@@ -47,7 +51,7 @@
       . ' "example3.com" is trusted, but domain "*.example3.com" not)',
 
     # 9 http://example3.com/
-    'aHR0cDovL2V4YW1wbGUzLmNvbS8K' => 1,
+    'aHR0cDovL2V4YW1wbGUzLmNvbS8K' => 'http://example3.com/',
     'Undeclared virtual host with trusted domain name',
 
     # 10 http://t.example.com/test
@@ -85,6 +89,21 @@
     'aHR0cHM6Ly90ZXN0MS5leGFtcGxlLmNvbTp0ZXN0QGhhY2tlci5jb20=' => 0,
     'userinfo trick',
 
+    # 22 url=https://hacker.com\@@test1.example.com/
+    'aHR0cHM6Ly9oYWNrZXIuY29tXEBAdGVzdDEuZXhhbXBsZS5jb20v' =>
+      'https://hacker.com%5C@@test1.example.com/',
+    'Good reencoding (2931)',
+
+    # 23 url=https://hacker.com:\@@test1.example.com/
+    'aHR0cHM6Ly9oYWNrZXIuY29tOlxAQHRlc3QxLmV4YW1wbGUuY29tLw==' =>
+      'https://hacker.com:%5C@@test1.example.com/',
+    'Good reencoding (2931)',
+
+    # 24 url='https://hacker.com\anything@test1.example.com/'
+    'aHR0cHM6Ly9oYWNrZXIuY29tXGFueXRoaW5nQHRlc3QxLmV4YW1wbGUuY29tLw==' =>
+      'https://hacker.com%5Canything@test1.example.com/',
+    'Good reencoding (2931)',
+
     # LOGOUT TESTS
     'LOGOUT',
 
@@ -95,7 +114,7 @@
 
     # 19 url=http://www.toto.com/, good referer
     'aHR0cDovL3d3dy50b3RvLmNvbS8=',
-    'http://test1.example.com/' => 1,
+    'http://test1.example.com/' => 'http://www.toto.com/',
     'Logout required by good site',
 
     # 20 url=http://www?<script>, good referer
@@ -130,10 +149,13 @@
         ),
         $detail
     );
-    ok( ( $res->[0] == ( $redir ? 302 : 200 ) ),
-        ( $redir ? 'Get redirection' : 'Redirection dropped' ) )
-      or explain( $res->[0], ( $redir ? 302 : 200 ) );
-    count(2);
+    if ($redir) {
+        expectRedirection( $res, $redir );
+    }
+    else {
+        expectOK($res);
+    }
+    count(1);
 }
 
 while ( defined( my $url = shift(@tests) ) ) {
@@ -151,9 +173,12 @@
         ),
         $detail
     );
-    ok( ( $res->[0] == ( $redir ? 302 : 200 ) ),
-        ( $redir ? 'Get redirection' : 'Redirection dropped' ) )
-      or explain( $res->[0], ( $redir ? 302 : 200 ) );
+    if ($redir) {
+        expectRedirection( $res, $redir );
+    }
+    else {
+        expectOK($res);
+    }
     ok(
         $res = $client->_post(
             '/',
@@ -164,7 +189,7 @@
     );
     expectOK($res);
     $id = expectCookie($res);
-    count(3);
+    count(2);
 }
 
 clean_sessions();
